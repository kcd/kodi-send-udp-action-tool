import sys
import socket
from struct import pack
import time

def build_action(message:str) -> str:
    """
         -----------------------------
         | -H1 Signature ("XBMC")    | - 4  x CHAR                4B
         | -H2 Version (eg. 2.0)     | - 2  x UNSIGNED CHAR       2B
         | -H3 PacketType            | - 1  x UNSIGNED SHORT      2B
         | -H4 Sequence number       | - 1  x UNSIGNED LONG       4B
         | -H5 No. of packets in msg | - 1  x UNSIGNED LONG       4B
         | -H7 Client's unique token | - 1  x UNSIGNED LONG       4B
         | -H8 Reserved              | - 10 x UNSIGNED CHAR      10B
         |---------------------------|
         | -P1 payload               | -
         -----------------------------
         https://github.com/xbmc/xbmc/blob/70d6b0c2e5a63e8076f7e9cf26331c2441ada371/tools/EventClients/lib/python/xbmcclient.py#L133
    """
    # https://docs.python.org/3/library/struct.html#format-characters 
    # ! network (= big-endian)
    # H u16
    # I u32

    payload = chr(0x01).encode('utf-8') # Action Buildin
    payload += message.encode('utf-8') + b"\0"
    # python 3 bytes(message, "utf-8")

    header = b"XBMC"
    header += chr(2).encode()
    header += chr(0).encode()
    header += pack("!H", 0x0A) # Action
    header += pack ("!I",1)
    header += pack ("!I",1)
    header += pack("!H", len(payload))
    header += pack ("!I",(int)(time.time()))
    header += b"\0" * 10
        
    return header + payload

def main(args) -> int:
    udp_ip ="192.168.1.44"
    udp_port = 9777
    message = "CECActivateSource()"

    if(len(args) < 2 or len(args) > 3):
        raise SystemExit(f"Usage: {args[0]} IP [action]")

    if(len(args) > 1):
        udp_ip = args[1]
    if(len(args) > 2):
        message = args[2]

    print(f"UDP target IP: {udp_ip} port: {udp_port}")
    print(f"XBMC Action: {message}")

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
    packet = build_action(message)
    sock.sendto(packet, (udp_ip, udp_port))
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))