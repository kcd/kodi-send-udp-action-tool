# Kodi Send UDP Action tool

Script to send UDP packet to Kodi media centre created to call a CEC function that seems to be unavailable on the JSONRPC API.

This can be used to turn on attached HDMI devices, namely the TV.

Execution

    python kodi_send_udp_action.py 192.168.1.50

Tests

    python -m pytest test_kodi_send_udp_action.py -rP
