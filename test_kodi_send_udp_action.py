import pytest
from kodi_send_udp_action import *
from mock import patch

@patch('time.time')
def test_build_action(mock_time):
    mock_time.return_value = 3155713200

    mock_action = "TestAction()"
    packet = build_action(mock_action)
    print(packet)
    assert packet == b'XBMC\x02\x00\x00\n\x00\x00\x00\x01\x00\x00\x00\x01\x00\x0e\xbc\x18\\\xb0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01TestAction()\x00'


@pytest.mark.parametrize("expected_ip", ['192.168.128.128', '192.168.128.64'])
@pytest.mark.parametrize("expected_action", ['ActionX', None])
@patch('socket.socket')
@patch('kodi_send_udp_action.build_action') # note patch decorator order matters and is unituitively right to left
def test_main_with_args(mock_build_action, MockSocket, expected_ip, expected_action):
    stub_packet = b'XBMC\x00\x01\x02'
    args = ['stub_file_path', expected_ip]
    if expected_action:
        args.append(expected_action)
    else:
        expected_action = 'CECActivateSource()' # default

    mock_socket_send = MockSocket.return_value.sendto # instance method to test
    mock_build_action.return_value = stub_packet
    
    exit_code = main(args)

    assert mock_build_action.call_count == 1
    mock_build_action.assert_called_with(expected_action)

    assert mock_socket_send.call_count == 1
    mock_socket_send.assert_called_with(stub_packet, (expected_ip, 9777))

    assert exit_code == 0